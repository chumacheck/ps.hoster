<?php

namespace App\Http\Controllers;

use App\Domain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class CabinetController extends Controller
{
    public function cabinet (Request $request)
    {
        $title = 'Cabinet';
        $user = Auth::user()->id;
        $domains =  $domains = $this->getRowsByUserID($user);;
        return view('cabinet.index', compact('title', 'user', 'domains'));
    }

    public function checkDomain (Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'domain' => 'required',
        ]);

        $domain = $request->domain;
        $userId = $request->user_id;
        $title = 'Cabinet';
        $apiKey = env('API_KEY');

        Domain::create([
            'user_id' => $userId,
            'domain' => $domain,
        ]);

        $response = Http::get('https://www.who-hosts-this.com/API/Host', [
            'key' => $apiKey,
            'url' => $domain,
        ]);

        if ($response->ok() && !$response->serverError() && !$response->clientError()) {
            $results = $response->object()->results;
            return view('cabinet.index', compact('title', 'results'));
        } else {
            $user = Auth::user()->id;
            $domains =  $domains = $this->getRowsByUserID($user);;
            return view('cabinet.index', compact('title', 'user', 'domains'));
        }
    }
    public function deleteDomain (Request $request)
    {
        $domainID = $request->id;
        $deleterElement = Domain::where('id', $domainID)->delete();
        if ($deleterElement) {
            $request->session()->flash('success', 'You have deleted element from your history');
            return redirect()->route('cabinet');
        } else {
            $request->session()->flash('error', 'Some problem occurred, try again');
            return redirect()->route('cabinet');
        }
    }
    public function editElement (Request $request)
    {
        $domainID = $request->id;
        $editElement = Domain::where('id', $domainID)->get()[0];
        $title = 'Edit';
        return view('cabinet.edit', compact('title', 'editElement'));
    }

    public function saveElement (Request $request)
    {
        $credentials = $request->only('id', 'domain');
        $element =Domain::find($credentials['id']);
        $element->domain = $credentials['domain'];
        $element->save();

        $title = 'Cabinet';
        $user = Auth::user()->id;
        $domains = $this->getRowsByUserID($user);
        $request->session()->flash('success', 'You have changed element');
        return view('cabinet.index', compact('title', 'user', 'domains'));
    }

    public function getRowsByUserID ($userID)
    {
        $domains = Domain::where('user_id', $userID)
            ->orderBy('created_at', 'desc')
            ->get();

        return $domains;
    }
}
