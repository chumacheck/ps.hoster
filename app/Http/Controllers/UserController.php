<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function create ()
    {
        $title = 'Registration';
        return view('user.create', compact('title'));
    }

    public function store (Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $request->session()->flash('success', 'You have registered successfully');
        Auth::login($user, true);

        return redirect()->route('home');
    }

    public function loginForm ()
    {
        $title = 'Login';
        return view('user.login', compact('title'));
    }

    public function login (Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('cabinet');
        }

        return redirect()->back()->with('error', 'Incorrect login or password');
    }

    public function logout (Request $request)
    {
        Auth::logout();
        return redirect()->route('login.create');
    }
}
