@extends('layouts.main')

@section('title') @parent {{ $title }} @endsection

@section('content')
    <main class="form-signin home-form">
        <h2>Можем изменить только одно поле</h2>
        <form method="POST" action="{{route('saveElement')}}" class="mt40">
            @csrf
            <input type="hidden" class="form-control" value="{{ $editElement->id }}" id="id" name="id">

            <div class="form-floating">
                <input type="text" class="form-control" id="domain" name="domain" value="{{ $editElement->domain }}">
            </div>
            <div class="form-floating">
                <input type="text" class="form-control" id="created" name="created" value="{{ $editElement->created_at }}" readonly>
            </div>

            <button type="submit" class="btn btn-primary mt40">Меняем</button>
        </form>
    </main>
@endsection
