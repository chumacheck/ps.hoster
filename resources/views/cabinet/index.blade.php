@extends('layouts.main')

@section('title') @parent {{ $title }} @endsection

@section('content')
    <main class="form-signin home-form">
        <h1>Хочешь проверить домен?</h1>
        <form method="POST" action="{{route('checkDomain')}}" class="mt40">
            @csrf
            <div class="form-floating">
                <input type="text" class="form-control" id="domain" name="domain" placeholder="google.com" value="">
                <label for="domain">Проверить домен</label>
            </div>
            <input type="hidden" class="form-control" value="{{ Auth::user()->id }}" id="user_id" name="user_id">
            <button type="submit" class="btn btn-primary search-btn">Вперед</button>
        </form>
        @if(isset($results))
            @foreach ($results as $result)
                <ul class="list-group blur">
                    <li class="list-group-item">
                        <p>ip - {{ $result->ip }}</p>
                    </li>
                    <li class="list-group-item">
                        <p>type - {{ $result->type }}</p>
                    </li>
                    <li class="list-group-item">
                        <p>isp_id - {{ $result->isp_id }}</p>
                    </li>
                        <li class="list-group-item">
                        <p>isp_name - {{ $result->isp_name }}</p>
                    </li>
                    <li class="list-group-item">
                        <p>isp_url - {{ $result->isp_url }}</p>
                    </li>
                </ul>
            @endforeach
            <a class="nav-link" href="{{ route('cabinet') }}">Посмотреть историю поиска</a>
        @endif
        @if(isset($domains))
            <h3>Твоя история</h3>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Домен</th>
                    <th scope="col">Дата поиска</th>
                    <th scope="col">Редактировать</th>
                    <th scope="col">Удалить</th>
                </tr>
                </thead>
                <tbody>
            @foreach ($domains as $domain)
                    <tr>
                        <th scope="row">{{ $domain->domain }}</th>
                        <td>{{ $domain->created_at }}</td>
                        <td>
                            <form method="GET" action="{{ route('editHistoryElement', ['id' => $domain->id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-primary">Изменяем</button>
                            </form>
                        </td>
                        <td>
                            <form method="GET" action="{{ route('deleteHistoryElement', ['id' => $domain->id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-primary">Удаляем</button>
                            </form>
                        </td>
                    </tr>
            @endforeach
                </tbody>
            </table>
        @endif
    </main>
@endsection
