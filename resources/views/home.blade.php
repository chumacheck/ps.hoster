@extends('layouts.main')

@section('title') @parent {{ $title }} @endsection

@section('content')
    <main class="form-signin home-form">
        <h1>Привет! <br>Это главная страничка сервиса</h1>
        @guest
            <h2>Зарегистрируйся, чтобы посмотреть функционал</h2>
        @endguest
        @auth
            <a class="w-100 btn btn-lg btn-primary" href="{{ route('cabinet') }}">Посмотреть историю поиска в личном кабинете</a>
        @endauth
    </main>
@endsection
