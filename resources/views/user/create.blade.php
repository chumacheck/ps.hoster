@extends('layouts.main')

@section('title') @parent {{ $title }} @endsection

@section('content')

    <main class="form-signin">
        <form action="{{ route('register.store') }}" method="post" class="registration__form">
            @csrf
            <h1 class="h3 mb-3 fw-normal">Зарегистрируйся</h1>

            <div class="form-floating">
                <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Igor" value="{{ old('name') }}">
                <label for="name">Имя</label>
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-floating">
                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="name@example.com" value="{{ old('email') }}">
                <label for="email">Почта</label>
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-floating">
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Password">
                <label for="password">Пароль</label>
                @error('password')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-floating">
                <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                <label for="password_confirmation">Подтверди пароль</label>
                @error('password_confirmation')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <button class="w-100 btn btn-lg btn-primary" type="submit">Зарегистрироваться</button>
            <a class="" href="{{ route('login.create') }}">Авторизуйся, если уже есть аккаунт</a>
        </form>
    </main>

@endsection
