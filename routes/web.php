<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Route::get('/register', 'UserController@create')->name('register.create');
Route::post('/register', 'UserController@store')->name('register.store');

Route::get('/login', 'UserController@loginForm')->name('login.create');
Route::post('/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout')->name('logout')->middleware('auth');

Route::get('/cabinet', 'CabinetController@cabinet')->name('cabinet')->middleware('auth');
Route::post('/cabinet', 'CabinetController@checkDomain')->name('checkDomain')->middleware('auth');
Route::get('/cabinet/{id}', 'CabinetController@deleteDomain')->name('deleteHistoryElement');
Route::get('/cabinet/edit/{id}', 'CabinetController@editElement')->name('editHistoryElement');
Route::post('/cabinet/edit', 'CabinetController@saveElement')->name('saveElement');
